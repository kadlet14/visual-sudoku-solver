import functions as f
import pickle
import os
import numpy as np
import matplotlib.pyplot as plt


def graph_sudoku_trn(results_dir_path, model_name):
    x = [
        [0],
        [0],
        [0],
        [0],
        [0],
        [0],
    ]
    y_trn = [
        [100],
        [100],
        [100],
        [100],
        [100],
        [100],
    ]

    for i, msp in enumerate([0]):
        for j, trs in enumerate([10, 50, 100, 200, 500, 1000]):
            for lbd in [0.1]:
                for k, epoch in enumerate(range(100, 5001, 100)):
                    try:
                        with open(os.path.join(results_dir_path, "trn_" + model_name + "_" + str(trs) + "_" + str(lbd) + "_" + str(msp) +
                                                                 "_" + str(epoch) + ".pkl"), 'rb') as pickle_file:
                            curr_res = pickle.load(pickle_file)
                            y_trn[j].append(curr_res[3]/trs*100)
                            x[j].append(epoch)
                    except:
                        continue

    x[5].append(300)
    y_trn[5].append(19/412*100)

    def visualize_sudoku_trn(x, y_trn):

        fig, trn = plt.subplots(1)

        # trn.plot(x, y_trn[0], 'k', x, y_trn[1], 'b', x, y_trn[2], 'g', x, y_trn[3], 'c')
        trn.plot(x[0], y_trn[0], 'k', x[1], y_trn[1], 'r', x[2], y_trn[2], 'g', x[3], y_trn[3], 'b', x[4], y_trn[4],
                 'c', x[5], y_trn[5], 'm')
        trn.set_title("NN-MIL-M3N-LP")
        trn.set(xlabel='epochs', ylabel='training 0/1 error [%]')
        trn.legend(["10 examples", "50 examples", "100 examples", "200 examples", "500 examples", "1000 examples"],
                   loc='upper right',
                   prop={'size': 5}, labelspacing=0.1, fontsize=10, handlelength=3.5)

        fig.tight_layout(pad=3.0)
        trn.grid()
        plt.show()

    visualize_sudoku_trn(x, y_trn)


def graph_hmc(results_dir_path, model_name1, model_name2):
    x = [10, 50, 100, 200, 500, 1000]
    y_trn = [
        [100] * 6,  # 0.0
        [100] * 6,  # 0.1
        [100] * 6,  # 0.2
        [100] * 6   # 0.5
    ]
    y_tst = [
        [100] * 6,  # 0.0
        [100] * 6,  # 0.1
        [100] * 6,  # 0.2
        [100] * 6   # 0.5
    ]
    y_trn_2 = [
        [100] * 6,  # 0.0
        [100] * 6,  # 0.1
        [100] * 6,  # 0.2
        [100] * 6  # 0.5
    ]
    y_tst_2 = [
        [100] * 6,  # 0.0
        [100] * 6,  # 0.1
        [100] * 6,  # 0.2
        [100] * 6  # 0.5
    ]
    y_fb = [20, 20, 20, 20, 20, 20]
    y_map = [25, 25, 25, 25, 25, 25]

    for i, msp in enumerate([0, 0.1, 0.2, 0.5]):
        for j, trs in enumerate([10, 50, 100, 200, 500, 1000]):
            res = [100, 100, 100]
            for lbd in [0.1, 1, 10, 100]:
                with open(os.path.join(results_dir_path, model_name1 + "_" + str(trs) + "_" + str(lbd) + "_" + str(msp) + ".pkl"), 'rb') as pickle_file:
                    curr_res = pickle.load(pickle_file)
                if curr_res[1] < res[1]:
                    res = curr_res
            y_trn[i][j] = res[0]
            y_tst[i][j] = res[2]

    for i, msp in enumerate([0, 0.1, 0.2, 0.5]):
        for j, trs in enumerate([10, 50, 100, 200, 500, 1000]):
            res = [100, 100, 100]
            for lbd in [0.1, 1, 10, 100]:
                with open(os.path.join(results_dir_path, model_name2 + "_" + str(trs) + "_" + str(lbd) + "_" + str(msp) + ".pkl"), 'rb') as pickle_file:
                    curr_res = pickle.load(pickle_file)
                if curr_res[1] < res[1]:
                    res = curr_res
            y_trn_2[i][j] = res[0]
            y_tst_2[i][j] = res[2]

    def visualize_hmc(x, y_trn, y_tst, y_trn_NN, y_tst_NN, y_map, y_fb):

        fig, ((trn, trn_NN), (tst, tst_NN)) = plt.subplots(2, 2, figsize=(7, 7))

        trn.plot(x, y_trn[0], 'k', x, y_trn[1], 'b', x, y_trn[2], 'g', x, y_trn[3], 'c', x, y_map, 'r--', x, y_fb, 'r')
        trn.set_title("MIL-M3N")
        trn.set_xscale('log')
        trn.set(xlabel='training examples', ylabel='training partial error [%]')
        trn.set_ylim([0, 40])
        trn.set_xlim([10, 1000])
        trn.legend(["0 % missing", "10 % missing", "20 % missing", "50 % missing", "MAP", "FB"], loc='upper right',
                   prop={'size': 5}, labelspacing=0.1, fontsize=10, handlelength=3.5)
        trn.grid()

        tst.plot(x, y_tst[0], 'k', x, y_tst[1], 'b', x, y_tst[2], 'g', x, y_tst[3], 'c', x, y_map, 'r--', x, y_fb, 'r')
        # tst.set_title("Testing")
        tst.set_xscale('log')
        tst.set(xlabel='training examples', ylabel='test error [%]')
        tst.set_ylim([20, 30])
        tst.set_xlim([10, 1000])
        tst.legend(["0 % missing", "10 % missing", "20 % missing", "50 % missing", "MAP", "FB"], loc='upper right',
                   prop={'size': 5}, labelspacing=0.1, fontsize=10, handlelength=3.5)
        tst.grid()

        trn_NN.plot(x, y_trn_NN[0], 'k', x, y_trn_NN[1], 'b', x, y_trn_NN[2], 'g', x, y_trn_NN[3], 'c', x, y_map, 'r--',
                    x, y_fb, 'r')
        trn_NN.set_title("NN-MIL-M3N")
        trn_NN.set_xscale('log')
        trn_NN.set(xlabel='training examples', ylabel='training partial error [%]')
        trn_NN.set_ylim([0, 40])
        trn_NN.set_xlim([10, 1000])
        trn_NN.legend(["0 % missing", "10 % missing", "20 % missing", "50 % missing", "MAP", "FB"], loc='upper right',
                      prop={'size': 5}, labelspacing=0.1, fontsize=10, handlelength=3.5)
        trn_NN.grid()

        tst_NN.plot(x, y_tst_NN[0], 'k', x, y_tst_NN[1], 'b', x, y_tst_NN[2], 'g', x, y_tst_NN[3], 'c', x, y_map, 'r--',
                    x, y_fb, 'r')
        # tst_NN.set_title("Testing")
        tst_NN.set_xscale('log')
        tst_NN.set(xlabel='training examples', ylabel='test error [%]')
        tst_NN.set_ylim([20, 30])
        tst_NN.set_xlim([10, 1000])
        tst_NN.legend(["0 % missing", "10 % missing", "20 % missing", "50 % missing", "MAP", "FB"], loc='upper right',
                      prop={'size': 5}, labelspacing=0.1, fontsize=10, handlelength=3.5)
        tst_NN.grid()

        fig.tight_layout(pad=3.0)
        plt.show()

    visualize_hmc(x, y_trn, y_tst, y_trn_2, y_tst_2, y_map, y_fb)


def graph_weights(models_dir_path):
    trn_len = 1000
    lbd = 0.1
    msp = 0

    model_weights_name = "model_" + str(trn_len) + "_" + str(lbd) + "_" + str(msp)
    model_path = os.path.join(models_dir_path, model_weights_name)

    weights_un = np.load(model_path + "_un.npz")
    weights_bin = np.load(model_path + "_bin.npz")

    file_names_un = weights_un.files
    file_names_bin = weights_bin.files

    w_a = weights_un[file_names_un[0]]
    w_b = weights_bin[file_names_bin[0]]

    weights_un2 = np.load(model_path + "_un_NN.npz")
    weights_bin2 = np.load(model_path + "_bin_NN.npz")

    file_names_un2 = weights_un2.files
    file_names_bin2 = weights_bin2.files

    w_a2 = weights_un2[file_names_un2[0]]
    w_b2 = weights_bin2[file_names_bin2[0]]

    def visualize_hmc_weights(w1, w2, w3, w4, d1, d2):
        fig, ((a1, a2), (a3, a4)) = plt.subplots(2, 2)
        im1 = a1.imshow(w1)
        a1.set_title("MIL-M3N")
        a1.set(ylabel='unary')

        im2 = a2.imshow(w2)
        a2.set_title("NN-MIL-M3N")
        a2.set(ylabel='unary')

        im3 = a3.imshow(w3)
        a3.set(ylabel='binary')

        im4 = a4.imshow(w4)
        a4.set(ylabel='binary')

        """for i in range(d1):
            for j in range(d2):
                text = ax.text(j, i, round(w_unary[i, j], 3), size=2.5, ha="center", va="center", color="r")"""

        plt.show()

    visualize_hmc_weights(w_a, w_a2, w_b, w_b2, 30, 30)


def graph_sudoku(results_dir_path, model_name):
    x = [10, 50, 100, 200, 500, 1000]
    y_tst = [
        [1000] * 6,  # 0.0
        [1000] * 6,  # 0.1
        [1000] * 6,  # 0.2
        [1000] * 6   # 0.5
    ]

    # y_SAT = [100 - 63.2]*6
    y_LEN = [100 - 72.35]*6

    for i, msp in enumerate([0, 0.1, 0.2, 0.5]):
        for j, trs in enumerate([10, 50, 100, 200, 500, 1000]):
            res = [1000, 1000, 1000, 1000, 1000, 1000]
            best_lbd = -1
            for lbd in [0.1, 0.3, 0.5, 0.9]:
                for epoch in range(100, 5001, 100):
                    try:
                        with open(os.path.join(results_dir_path, model_name + "_" + str(trs) + "_" + str(lbd) + "_" + str(msp) +
                                                                 "_" + str(epoch) + ".pkl"), 'rb') as pickle_file:
                            curr_res = pickle.load(pickle_file)
                    except:
                        continue
                    if curr_res[3] < res[3]:
                        res = curr_res
                        best_lbd = lbd
            print(trs, msp, "..............")
            print(best_lbd, res)
            y_tst[i][j] = res[5]

    def visualize_sudoku(x, y_tst, y_LEN):

        fig, tst = plt.subplots(1)


        # tst.set_title("Testing")
        tst.set_xscale('log')
        tst.set_title("NN-MIL-M3N-LP")
        tst.plot(x, y_tst[0], 'k', x, y_tst[1], 'b', x, y_tst[2], 'g', x, y_tst[3], 'c', x, y_LEN, 'r--')
        tst.set(xlabel='training examples', ylabel='test 0/1 error [%]')
        tst.legend(["0 % missing", "10 % missing", "20 % missing", "50 % missing", "LeNet + solver"], loc='upper right',
                   prop={'size': 5}, labelspacing=0.1, fontsize=10, handlelength=3.5)

        fig.tight_layout(pad=3.0)
        tst.grid()
        plt.show()

    visualize_sudoku(x, y_tst, y_LEN)


def graph_sudoku_symbolic(results_dir_path, model_name, model_name2):
    x = [10, 50, 100, 200, 500, 1000]
    y_tst1 = [
        [1000] * 6,  # 0.0
        [1000] * 6,  # 0.1
        [1000] * 6,  # 0.2
        [1000] * 6   # 0.5
    ]

    y_tst2 = [
        [1000] * 6,  # 0.0
        [1000] * 6,  # 0.1
        [1000] * 6,  # 0.2
        [1000] * 6  # 0.5
    ]

    for i, msp in enumerate([0, 0.1, 0.2, 0.5]):
        for j, trs in enumerate([10, 50, 100, 200, 500, 1000]):
            res = [1000, 1000, 1000, 1000, 1000, 1000]
            best_lbd = -1
            for lbd in [0]:
                for epoch in range(1000, 5001, 100):
                    try:
                        with open(os.path.join(results_dir_path, model_name + "_" + str(trs) + "_" + str(lbd) + "_" + str(msp) +
                                                                 "_" + str(epoch) + ".pkl"), 'rb') as pickle_file:
                            curr_res = pickle.load(pickle_file)
                    except:
                        continue
                    if curr_res[3] <= res[3]:
                        res = curr_res
                        best_lbd = lbd
            print(trs, msp, "..............")
            print(best_lbd, res)
            y_tst1[i][j] = res[3]

    for i, msp in enumerate([0, 0.1, 0.2, 0.5]):
        for j, trs in enumerate([10, 50, 100, 200, 500, 1000]):
            res = [1000, 1000, 1000, 1000, 1000, 1000]
            best_lbd = -1
            for lbd in [0, 0.1, 1, 10]:
                try:
                    with open(os.path.join(results_dir_path, model_name2 + "_" + str(trs) + "_" + str(lbd) + "_" + str(msp) + ".pkl"), 'rb') as pickle_file:
                        curr_res = pickle.load(pickle_file)
                        print(curr_res)
                except:
                    continue
                if curr_res[2] <= res[2]:
                    res = curr_res
                    best_lbd = lbd
            print(trs, msp, "..............")
            print(best_lbd, res)
            y_tst2[i][j] = res[0]

    def visualize_sudoku_sym(x, y_tst1, y_tst2):

        fig, (tst1, tst2) = plt.subplots(2)


        # tst.set_title("Testing")
        tst1.set_xscale('log')
        tst1.set_title("MIL-M3N-LP")
        tst1.plot(x, y_tst1[0], 'k', x, y_tst1[1], 'b', x, y_tst1[2], 'g', x, y_tst1[3], 'c')
        tst1.set(xlabel='training examples', ylabel='test 0/1 error [%]')
        tst1.legend(["0 % missing", "10 % missing", "20 % missing", "50 % missing", "LeNet + solver"], loc='upper right',
                   prop={'size': 10}, labelspacing=0.1, fontsize=10, handlelength=3.5)

        # tst.set_title("Testing")
        tst2.set_xscale('log')
        tst2.set_title("NN-MIL-M3N-LP")
        tst2.plot(x, y_tst2[0], 'k', x, y_tst2[1], 'b', x, y_tst2[2], 'g', x, y_tst2[3], 'c')
        tst2.set(xlabel='training examples', ylabel='test 0/1 error [%]')
        tst2.legend(["0 % missing", "10 % missing", "20 % missing", "50 % missing", "LeNet + solver"], loc='upper right',
                   prop={'size': 10}, labelspacing=0.1, fontsize=10, handlelength=3.5)

        fig.tight_layout(pad=3.0)
        tst1.grid()
        tst2.grid()
        plt.show()

    visualize_sudoku_sym(x, y_tst1, y_tst2)


def graph_weights_symbolic(models_dir_path):
    trn_len = 1000
    lbd = 0
    msp = 0

    model_weights_name = "M3N_relaxed_" + str(trn_len) + "_" + str(lbd) + "_" + str(msp)
    model_weights_name2 = "NN_M3N_" + str(trn_len) + "_" + str(lbd) + "_" + str(msp)
    model_path = os.path.join(models_dir_path, model_weights_name)
    model_path2 = os.path.join(models_dir_path, model_weights_name2)

    weights_un = np.load(model_path + "_un_epoch_100.npz")
    weights_bin = np.load(model_path + "_bin_epoch_100.npz")

    file_names_un = weights_un.files
    file_names_bin = weights_bin.files

    w_a = weights_un[file_names_un[0]]
    w_b = weights_bin[file_names_bin[0]]

    weights_un2 = np.load(model_path2 + "_un_epoch_100.npz")
    weights_bin2 = np.load(model_path2 + "_bin_epoch_100.npz")

    file_names_un2 = weights_un2.files
    file_names_bin2 = weights_bin2.files

    w_a2 = weights_un2[file_names_un2[0]]
    w_b2 = weights_bin2[file_names_bin2[0]]

    def visualize_sudoku_weights(w1, w2, w3, w4):
        fig, ((a1, a2), (a3, a4)) = plt.subplots(2, 2)
        im1 = a1.imshow(w1)
        a1.set_title("MIL-M3N")
        a1.set(ylabel='unary')

        im2 = a2.imshow(w2)
        a2.set_title("NN-MIL-M3N")
        a2.set(ylabel='unary')

        im3 = a3.imshow(w3)
        a3.set(ylabel='binary')

        im4 = a4.imshow(w4)
        a4.set(ylabel='binary')

        for i in range(9):
            for j in range(10):
                text = a1.text(j, i, round(w1[i, j], 3), size=2.5, ha="center", va="center", color="r")
                text = a2.text(j, i, round(w2[i, j], 3), size=2.5, ha="center", va="center", color="r")

        for i in range(9):
            for j in range(9):
                text = a3.text(j, i, round(w3[i, j], 3), size=2.5, ha="center", va="center", color="r")
                text = a4.text(j, i, round(w4[i, j], 3), size=2.5, ha="center", va="center", color="r")

        fig.tight_layout(pad=0.0)
        plt.show()


    visualize_sudoku_weights(w_a, w_a2, w_b, w_b2)


if __name__ == "__main__":
    dir_path = os.path.dirname(__file__)
    results_dir_path_abs = os.path.join(dir_path, "results")
    models_dir_path_abs = os.path.join(dir_path, "models")

    # graph_hmc(results_dir_path_abs, model_name1="M3N", model_name2="NN_M3N")
    # graph_weights(models_dir_path_abs)
    # graph_sudoku_trn(results_dir_path_abs, "NN_M3N_lenet")
    # graph_sudoku(results_dir_path_abs + "_soft_3", "NN_M3N_lenet")
    # graph_sudoku_symbolic(results_dir_path_abs, "M3N_relaxed", "NN_M3N")
    # graph_weights_symbolic(models_dir_path_abs)
