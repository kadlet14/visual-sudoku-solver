import numpy as np


class AdamOptimizer:

    def __init__(self, d1, d2):
        self.v_0 = np.zeros((d1, d2))
        self.q_0 = np.zeros((d1, d2))
        self.b1 = 0.9
        self.b2 = 0.999
        self.eps = 0.00000001

    def optimize(self, w, g, k, lr):
        v_1 = self.b1 * self.v_0 + (1 - self.b1) * g
        q_1 = self.b2 * self.q_0 + (1 - self.b2) * np.power(g, 2)

        v = np.divide(v_1, (1 - self.b1**k))
        q = np.divide(q_1, (1 - self.b2**k))

        w_updated = w - lr*(np.reciprocal(np.sqrt(q) + self.eps)) * v

        self.v_0 = v_1
        self.q_0 = q_1

        return w_updated
