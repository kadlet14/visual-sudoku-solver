# notes
#
# for this script to run correctly, adag solver must be installed first and LD_LIBRARY_PATH must be set accordingly
#
# LD_LIBRARY_PATH can be set using command: 
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path_to_program_folder/manet/adag_solver/


# imports

import Dataset
import TrainConfig
import GraphConfig
import NN_M3N_lenet
import functions as f
from manet.maxsum import adag

import os
import sys
import pickle
import numpy as np
from operator import itemgetter
import torch.nn.functional as F
import torch

# setup


# jobid - determines training configuration, can be set using slurm job array or manually

# jobid = int(os.getenv('SLURM_ARRAY_TASK_ID'))
jobid = 0

# paths
dir_path = os.path.abspath('')
model_dir_path = os.path.join(dir_path, "models")
data_dir_path = os.path.join(dir_path, "data")

# create directories
if not os.path.exists(model_dir_path):
    os.makedirs(model_dir_path)
    
# training_config
number_of_training_epochs = 500
validation_frequency = 100
trn_dataset_sizes = [10, 50, 100, 200, 500, 1000]
val_dataset_size = 100
tst_dataset_size = 100
missing_probs = [0, 0.1, 0.2, 0.5]
lambdas = [0.1, 0.3, 0.5, 0.9]
batch_size = 10
optimizer_type = "Adam"

# following 3 params can be set manually if not using slurm job array
trn_dataset_size = trn_dataset_sizes[jobid // (len(missing_probs)*len(lambdas))]
missing_prob = missing_probs[(jobid // len(lambdas)) % len(missing_probs)]
lbd = lambdas[jobid % len(missing_probs)]

train_config = TrainConfig.TrainConfig(number_of_training_epochs, validation_frequency, trn_dataset_size,
                                       val_dataset_size, tst_dataset_size, missing_prob, lbd, batch_size,
                                       optimizer_type)

# dataset

dataset = Dataset.Dataset(missing_prob, trn_dataset_size, val_dataset_size, tst_dataset_size)
dataset.load_pkl_visual(os.path.join(data_dir_path, "sudoku_1000_100_100_mis0_linear.pkl"))

# graph configuration

graph_config = None
edges = []

for v1 in range(81):
    row = v1 // 9
    col = v1 % 9
    for r in range(9):
        v2 = r + row*9
        if v1 != v2 and [v1, v2, 0] not in edges and [v2, v1, 0] not in edges:
            edges.append([v1, v2, 0])
    for c in range(9):
        v2 = col + c*9
        if v1 != v2 and [v1, v2, 0] not in edges and [v2, v1, 0] not in edges:
            edges.append([v1, v2, 0])

    sr = row // 3
    sc = col // 3
    v = sr*27 + sc*3
    for r in range(3):
        for c in range(3):
            v2 = v + c + r*9
            if v1 != v2 and [v1, v2, 0] not in edges and [v2, v1, 0] not in edges:
                edges.append([v1, v2, 0])

graph_config = GraphConfig.GraphConfig(objects_num=81, observations_num=10, labels_num=9, edges=edges)
edges_eval = np.transpose(np.array(edges))

# model

model = NN_M3N_lenet.NN_M3N_lenet(graph_config)

# set number of threads to 1 - turned out to be the fastest way, probably depends on used device
# relevant only for training on CPU (training on GPU didnt show performance increase)

torch.set_num_threads(1)
'''
if torch.cuda.is_available():
    # set device
    device = torch.device(0)
else:
    device = 'cpu'
'''
device = 'cpu'

# calc missing prob

missing_prob_clean = missing_prob

missing_cnt = 0
for (x, y, i) in dataset.trn_data:
    missing_cnt += y.tolist().count(-1)
missing_prob = missing_cnt / len(dataset.trn_data) / graph_config.objects_num

# create aux training params

fis_1 = []
fis_2 = []
for x, y, i in dataset.trn_data:

    fi_1 = (torch.rand((graph_config.labels_num, len(graph_config.edges)), requires_grad=False)-0.5).to(device)
    fi_2 = (torch.rand((graph_config.labels_num, len(graph_config.edges)), requires_grad=False)-0.5).to(device)

    fis_1.append(fi_1)
    fis_2.append(fi_2)

# init optimizer and register updated params

optimizer = torch.optim.Adam([
                    {'params': model.parameters()},
                    {'params': model.w_b},
                    {'params': fis_1},
                    {'params': fis_2},
                ], weight_decay=lbd)

# training loop

best_val_err_01 = np.PINF

for epoch in range(1, number_of_training_epochs + 1):

    # set training mode
    model.train()
    
    # update lr
    lr = 1 / (100 + epoch)
    for g in optimizer.param_groups:
        g['lr'] = lr

    # get batches
    batches = dataset.get_batches(train_config.batch_size)

    for batch in batches:
        for (x, y, i) in batch:
            
            # get aux params for current training example
            fi_1 = fis_1[i]
            fi_2 = fis_2[i]
            
            # turn grad of used aux params on
            fi_1.requires_grad_(True)
            fi_2.requires_grad_(True)
            
            # forward step
            Q, G = model(x, device)

            # calc loss
            loss = model.loss(Q, G, y, fi_1, fi_2, lbd, missing_prob, device)

            # backward step
            loss.backward()
            
            # turn grad of used aux params off
            fi_1.requires_grad_(False)
            fi_2.requires_grad_(False)
        
        # update params
        optimizer.step()
        
        # clear gradients
        optimizer.zero_grad(set_to_none=True)
        
    # periodically eval performance on validation set and save best model
    if epoch % train_config.validation_frequency == 0:
        
        val_err_01 = 0
        
        # eval on val dataset
        for (x, y) in dataset.val_data:
            Q, G = model(x)
            Q = Q.detach().numpy().astype(np.float64)
            G = G.detach().numpy().astype(np.float64)

            y_out, _ = adag(Q, G, edges_eval)
            hl = f.hamming_loss(y, y_out)
            
            if hl != 0:
                val_err_01 += 1
                
        # save model if its better
        if val_err_01 < best_val_err_01:
            model_save_name = "NN_M3N_Lenet" + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
                    train_config.missing_prob)
            model_path = os.path.join(model_dir_path, model_save_name)
            
            torch.save(model.state_dict(), model_path + ".pt")
            np.savez(model_path + "_bin", model.w_b.detach().numpy())

# test

# load weights
model_save_name = "NN_M3N_Lenet" + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
                    train_config.missing_prob)
model_path = os.path.join(model_dir_path, model_save_name)

weights_bin = np.load(model_path + "_bin.npz")
file_names_bin = weights_bin.files
w_b = weights_bin[file_names_bin[0]]
model.load_state_dict(torch.load(model_path + ".pt"))
model.w_b = w_b

# eval on test dataset

tst_err_01 = 0

with torch.no_grad():
    model.eval()

    for x, y in dataset.tst_data:
        Q, G = model(x)
        Q = Q.detach().numpy().astype(np.float64)

        y_out, en = adag(Q, G, edges_eval)
        hl = f.hamming_loss(y, y_out)
        
        if hl != 0:
            tst_err_01 += 1
        
tst_err_01 = tst_err_01 / len(dataset.tst_data) * 100

# print or save result...

print("test 0/1 error:", tst_err_01, " %")
