import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
import os
import functions as f
from manet.maxsum import adag
from manet.maxsum import viterbi
import time


class NN_M3N_lenet(torch.nn.Module):

    def __init__(self, graph_config):
        super().__init__()
        self.graph_config = graph_config
        self.w_b = torch.zeros((graph_config.labels_num, graph_config.labels_num), requires_grad=True)

        self.conv_1 = nn.Conv2d(1, 6, kernel_size=5, padding=2)
        self.conv_2 = nn.Conv2d(6, 16, kernel_size=5)
        self.conv_3 = nn.Conv2d(16, 120, kernel_size=5)

        self.fc1 = nn.Linear(120, 84)
        self.fc2 = nn.Linear(84, 9)

        for layer in self.modules():
            if type(layer) in [torch.nn.Conv2d]:
                torch.nn.init.xavier_uniform_(layer.weight)
            if type(layer) in [torch.nn.Linear]:
                torch.nn.init.xavier_uniform_(layer.weight)

    def move_device(self, device):
        self.w_b = self.w_b.to(device)

    def forward(self, x, device=None):

        Q = torch.zeros(self.graph_config.labels_num, self.graph_config.objects_num)
        if device:
            Q = Q.to(device)
        for v in range(self.graph_config.objects_num):
            xx = x[:, :, v].reshape(1, 1, 28, 28)
            xx = F.relu(self.conv_1(xx))
            xx = F.max_pool2d(xx, 2)
            xx = F.relu(self.conv_2(xx))
            xx = F.max_pool2d(xx, 2)
            xx = F.relu(self.conv_3(xx))
            xx = xx.view(-1, 120)
            xx = F.relu(self.fc1(xx))
            xx = self.fc2(xx)
            xx = torch.nn.functional.softmax(xx)
            Q[:, v] = xx

        G = self.w_b
        return Q, G

    def loss(self, Q, G, a, fi_1, fi_2, lbd, miss_prob, device):

        N = torch.zeros(self.graph_config.labels_num, self.graph_config.objects_num).to(device)

        u_vv = 0
        psi_vv = 0

        for i, e in enumerate(self.graph_config.edges):
            v1 = e[0]
            v2 = e[1]

            f1 = fi_1[:, i]
            f2 = fi_2[:, i]

            N[:, v1] += f1
            N[:, v2] += f2

            d1 = torch.reshape(f1, (self.graph_config.labels_num, 1)).repeat(1, self.graph_config.labels_num)
            d2 = torch.reshape(f2, (1, self.graph_config.labels_num)).repeat(self.graph_config.labels_num, 1)

            ind = torch.argmax(d1 + d2 + G).item()
            y1 = ind // self.graph_config.labels_num
            y2 = ind % self.graph_config.labels_num
            u_vv += f1[y1] + f2[y2] + G[y1, y2]

            y1 = a[v1]
            y2 = a[v2]

            if y1 != -1 and y2 != -1:
                psi_vv += G[y1, y2]

        L = torch.ones(self.graph_config.labels_num, self.graph_config.objects_num) / self.graph_config.objects_num

        for i, aa in enumerate(a):
            if aa != -1:
                L[aa, i] = 0

        u_v = torch.sum(torch.max(Q + L - N, 0)[0])

        psi_v = 0
        for i, aa in enumerate(a):
            if aa != -1:
                psi_v += Q[aa, i]

        psi_v /= (1 - miss_prob)
        psi_vv /= (1 - miss_prob) ** 2

        #norm = lbd/2*(torch.linalg.norm(Q)**2)  # + torch.linalg.norm(G)**2)

        loss = u_v + u_vv - psi_v - psi_vv #+ norm

        return loss
