"""
class defining classifier configurations
"""
class GraphConfig:

    def __init__(self, objects_num, observations_num, labels_num, edges):
        self.objects_num = objects_num
        self.observations_num = observations_num
        self.labels_num = labels_num
        self.edges = edges

