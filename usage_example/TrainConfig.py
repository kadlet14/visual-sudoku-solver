class TrainConfig:

    def __init__(self, number_of_training_epochs, validation_frequency, trn_dataset_size, val_dataset_size,
                 tst_dataset_size, missing_prob, lbd, batch_size, optimizer):
        self.number_of_training_epochs = number_of_training_epochs
        self.validation_frequency = validation_frequency
        self.trn_dataset_size = trn_dataset_size
        self.val_dataset_size = val_dataset_size
        self.tst_dataset_size = tst_dataset_size
        self.missing_prob = missing_prob
        self.lbd = lbd
        self.batch_size = batch_size
        self.optimizer = optimizer
