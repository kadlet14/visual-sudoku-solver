import SGD_optimizer
import functions as f
import Adam_optimizer
import GraphConfig
from manet.maxsum import adag
from manet.maxsum import viterbi

import os
import sys
import numpy
import numpy as np
import time
import pickle


class M3NClassifier:

    def __init__(self, graph_config):
        self.graph_config = graph_config
        self.w_un = np.zeros((graph_config.labels_num, graph_config.observations_num))
        self.w_bin = np.zeros((graph_config.labels_num, graph_config.labels_num))

    def save(self, path):
        print("saving model: ", path)
        np.savez(path + "_un", self.w_un)
        np.savez(path + "_bin", self.w_bin)

    def classify(self, x):
        Q = np.transpose([self.w_un[:, i] for i in x])
        G = self.w_bin
        y = self.viterbi(Q, G, np.zeros(self.graph_config.objects_num, dtype=int) - 1)
        return y

    def delta(self, x, y, missing_prob):
        y_opt, val = self.viterbi(x, y)
        v = val - np.dot(self.w_un, self.psi_partial_un(x, y, missing_prob)) - np.dot(self.w_bin, self.psi_partial_bin(x, y, missing_prob))
        return y_opt, v

    def psi_un(self, x, y):
        s_x = self.graph_config.observations_num
        s_y = self.graph_config.labels_num
        p = np.zeros((s_y, s_x), dtype=float)
        for xi, yi in zip(x, y):
            p[yi, xi] += 1
        return p

    def psi_bin(self, y):
        s_y = self.graph_config.labels_num
        p = np.zeros((s_y, s_y), dtype=float)
        for i in range(1, len(y)):
            y0 = y[i - 1]
            y1 = y[i]
            p[y0, y1] += 1
        return p

    def psi_partial_un(self, x, y, missing_prob):
        s_x = self.graph_config.observations_num
        s_y = self.graph_config.labels_num
        p = np.zeros((s_y, s_x), dtype=float)
        for xi, yi in zip(x, y):
            if yi != -1:
                p[yi, xi] += 1

        p /= (1 - missing_prob)

        return p

    def psi_partial_bin(self, y, missing_prob):
        s_y = self.graph_config.labels_num
        p = np.zeros((s_y, s_y), dtype=float)
        for i in range(1, len(y)):
            y0 = y[i - 1]
            y1 = y[i]
            if y0 != -1 and y1 != -1:
                p[y0, y1] += 1

        p /= (1 - missing_prob)**2

        return p

    def train(self, train_config, dataset, model_dir_path, model_name, results_dir_path):

        best_val_err = np.PINF
        best_val_err_01 = np.PINF
        best_trn_err = np.PINF
        best_trn_err_01 = np.PINF

        missing_cnt = 0
        for (x, y, i) in dataset.trn_data:
            missing_cnt += y.tolist().count(-1)
        missing_prob = missing_cnt / len(dataset.trn_data) / self.graph_config.objects_num

        if train_config.optimizer == "Adam":
            optimizer_un = Adam_optimizer.AdamOptimizer(self.graph_config.labels_num, self.graph_config.observations_num)
            optimizer_bin = Adam_optimizer.AdamOptimizer(self.graph_config.labels_num, self.graph_config.labels_num)

        elif train_config.optimizer == "SGD":
            optimizer_un = SGD_optimizer.SGDOptimizer()
            optimizer_bin = SGD_optimizer.SGDOptimizer()

        for epoch in range(1, train_config.number_of_training_epochs + 1):

            batches = dataset.get_batches(train_config.batch_size)

            for batch in batches:

                grad_un = []
                grad_bin = []

                for (x, y, i) in batch:
                    Q = np.transpose([self.w_un[:, i] for i in x])
                    G = self.w_bin
                    y_opt = self.viterbi(Q, G, y)
                    grad_un.append(self.psi_un(x, y_opt) - self.psi_partial_un(x, y, missing_prob))
                    grad_bin.append(self.psi_bin(y_opt) - self.psi_partial_bin(y, missing_prob))

                g_un = (train_config.lbd*self.w_un + sum(grad_un)/len(grad_un))
                g_bin = (train_config.lbd*self.w_bin + sum(grad_bin)/len(grad_bin))
                lr = 1 / (100 + epoch)

                self.w_un = optimizer_un.optimize(self.w_un, g_un, epoch, lr)
                self.w_bin = optimizer_bin.optimize(self.w_bin, g_bin, epoch, lr)

            # epoch ended

            if epoch % train_config.validation_frequency == 0:
                # f.visualize_weights2(self.w_un, 30, 30)
                # f.visualize_weights2(self.w_bin, 30, 30)
                print("epoch: ", epoch)

                # f.visualize_weights2(self.w_un, self.graph_config.labels_num, self.graph_config.observations_num)
                # f.visualize_weights2(self.w_bin, self.graph_config.labels_num, self.graph_config.labels_num)

                trn_err = 0
                trn_err_01 = 0
                val_err = 0
                val_err_01 = 0

                for (x, y, i) in dataset.trn_data:

                    y_out = self.classify(x)
                    hl = f.hamming_loss(y, y_out)
                    trn_err += hl

                    if hl != 0:
                        trn_err_01 += 1

                trn_err = 100 * trn_err / len(dataset.trn_data)
                trn_err_01 = 100 * trn_err_01 / len(dataset.trn_data)

                for (x, y) in dataset.val_data:
                    y_out = self.classify(x)
                    hl = f.hamming_loss(y, y_out)
                    val_err += hl

                    if hl != 0:
                        val_err_01 += 1

                val_err = 100 * val_err / len(dataset.val_data)
                val_err_01 = 100 * val_err_01 / len(dataset.val_data)
                
                print("training error: ", trn_err)
                print("training error 0/1: ", trn_err_01)
                print("validation error: ", val_err)
                print("validation error 0/1: ", val_err_01)
                print("best training error: ", best_trn_err)
                print("best training error 0/1: ", best_trn_err_01)
                print("best validation error: ", best_val_err)
                print("best validation error 0/1: ", best_val_err_01)

                save_model = False

                if trn_err < best_trn_err:
                    best_trn_err = trn_err

                if trn_err_01 < best_trn_err_01:
                    best_trn_err_01 = trn_err_01

                if val_err < best_val_err:
                    best_val_err = val_err
                    save_model = True

                if val_err_01 < best_val_err_01:
                    best_val_err_01 = val_err_01

                if save_model:
                    model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
                        train_config.missing_prob)
                    model_path = os.path.join(model_dir_path, model_save_name)
                    print(model_path)
                    self.save(model_path)

                res = [best_trn_err, best_trn_err_01, best_val_err, best_val_err_01, np.PINF, np.PINF]

                print("saving best result: ", res)

                with open(os.path.join(results_dir_path,
                                       model_name + "_" + str(train_config.trn_dataset_size) + "_" +
                                       str(train_config.lbd) + "_" + str(train_config.missing_prob) +
                                       ".pkl"), 'wb') as pickle_file:
                    pickle.dump(res, pickle_file)

        print("training finished")

        return best_trn_err, best_trn_err_01, best_val_err, best_val_err_01

    def viterbi(self, Q, G, y):
        labels_num, seq_len = Q.shape
        y_opt = []

        vals = np.zeros((labels_num, seq_len), dtype=float)
        dirs = np.zeros((labels_num, seq_len), dtype=int)

        hamm = np.zeros(labels_num)
        if y[0] != -1:
            hamm = np.ones(labels_num)/seq_len
            hamm[y[0]] = 0

        vals[:, 0] = (Q[:, 0] + hamm)

        for i in range(1, seq_len):
            curr_y = y[i]
            tran = G
            prev_vals = vals[:, i - 1]

            hamm = np.zeros(labels_num)
            if curr_y != -1:
                hamm = np.ones(labels_num)/seq_len
                hamm[curr_y] = 0
            emm = Q[:, i]

            s = tran + prev_vals.reshape(labels_num, 1)

            v = np.max(s, 0) + emm + hamm
            d = np.argmax(s, 0)

            vals[:, i] = v
            dirs[:, i] = d

        last_ind = np.argmax(vals[:, -1])
        y_opt.append(last_ind)

        for i in range(seq_len - 1):
            yi = dirs[y_opt[-1], seq_len - 1 - i]
            y_opt.append(yi)

        y_opt.reverse()

        return y_opt
