import GraphConfig
import Dataset
import HMC_generator
import FB_predictor
import M3N_linear_visual
import MAP_predictor
import M3N_classifier_relaxed
import M3N_classifier
import NN_M3N
import NN_M3N_visual
import NN_M3N_lenet
import TrainConfig
import functions as f
from manet.maxsum import adag
from manet.maxsum import viterbi

import time
import numpy
import os
import sys
import pickle
import random
import numpy as np
from operator import itemgetter
import torch.nn.functional as F
import torch
import matplotlib.pyplot as plt
from math import ceil

jobid = int(os.getenv('SLURM_ARRAY_TASK_ID'))
jobid = 80  # TODO hide
print(jobid)

task = "sudoku"  # "sudoku", "hmc"
input_type = "visual"  # "visual", "symbolic"
model_name = "NN_M3N_lenet"  # "M3N", "M3N_relaxed", "NN_M3N", "NN_M3N_visual", "NN_M3N_lenet", "M3N_linear_visual"
dir_path = os.path.dirname(__file__)
model_dir_path = os.path.join(dir_path, "models")
data_dir_path = os.path.join(dir_path, "data")
results_dir_path = os.path.join(dir_path, "results")
generate_data = False
train = False
test = False
test2 = True
test_trn = False

# training_config
number_of_training_epochs = 5000
validation_frequency = 100
trn_dataset_sizes = [10, 50, 100, 200, 500, 1000]
val_dataset_size = 100  # TODO adjust based on config
tst_dataset_size = 100  # TODO adjust based on config
missing_probs = [0, 0.1, 0.2, 0.5]
lambdas = [0, 1, 10, 100]  # TODO adjust based on config
batch_size = 10
optimizer_type = "Adam"  # "Adam", "SGD"

trn_dataset_size = trn_dataset_sizes[jobid // (len(missing_probs)*len(lambdas))]
missing_prob = missing_probs[(jobid // len(lambdas)) % len(missing_probs)]
lbd = lambdas[jobid % len(missing_probs)]

print("task: ", task)
print("input type: ", input_type)
print("model name: ", model_name)
print("number_of_training_epochs: ", number_of_training_epochs)
print("validation_frequency: ", validation_frequency)
print("trn_dataset_size: ", trn_dataset_size)
print("val_dataset_size: ", val_dataset_size)
print("tst_dataset_size: ", tst_dataset_size)
print("missing_prob: ", missing_prob)
print("lambda: ", lbd)
print("batch_size: ", batch_size)
print("omtimizer: ", optimizer_type)

train_config = TrainConfig.TrainConfig(number_of_training_epochs, validation_frequency, trn_dataset_size,
                                       val_dataset_size, tst_dataset_size, missing_prob, lbd, batch_size,
                                       optimizer_type)

graph_config = None
dataset = None
edges = []

if task == "hmc":

    if generate_data:
        hmc_gen = HMC_generator.HMCGenerator()
        data = []
        for i in range(trn_dataset_size + val_dataset_size + tst_dataset_size):
            x, y = hmc_gen.generate()
            data.append([x, y])
        with open(os.path.join(data_dir_path, "hmc.pkl"), 'wb') as pickle_file:
            pickle.dump(data, pickle_file)

    dataset = Dataset.Dataset(missing_prob, trn_dataset_size, val_dataset_size, tst_dataset_size)
    dataset.load_pkl(os.path.join(data_dir_path, "hmc.pkl"))

    for x in range(99):
        edges.append([x, x + 1, 0])

    graph_config = GraphConfig.GraphConfig(objects_num=100, observations_num=30, labels_num=30, edges=edges)

elif task == "sudoku":

    if input_type == "visual":
        dataset = Dataset.Dataset(missing_prob, trn_dataset_size, val_dataset_size, tst_dataset_size)
        dataset.load_pkl_visual(os.path.join(data_dir_path, "sudoku_1000_100_100_mis0_linear.pkl"))

    elif input_type == "symbolic":
        dataset = Dataset.Dataset(missing_prob, trn_dataset_size, val_dataset_size, tst_dataset_size)
        dataset.load_csv(os.path.join(data_dir_path, "sudoku.csv"))

    for v1 in range(81):
        row = v1 // 9
        col = v1 % 9
        for r in range(9):
            v2 = r + row*9
            if v1 != v2 and [v1, v2, 0] not in edges and [v2, v1, 0] not in edges:
                edges.append([v1, v2, 0])
        for c in range(9):
            v2 = col + c*9
            if v1 != v2 and [v1, v2, 0] not in edges and [v2, v1, 0] not in edges:
                edges.append([v1, v2, 0])

        sr = row // 3
        sc = col // 3
        v = sr*27 + sc*3
        for r in range(3):
            for c in range(3):
                v2 = v + c + r*9
                if v1 != v2 and [v1, v2, 0] not in edges and [v2, v1, 0] not in edges:
                    edges.append([v1, v2, 0])

    graph_config = GraphConfig.GraphConfig(objects_num=81, observations_num=10, labels_num=9, edges=edges)

edges_eval = np.transpose(np.array(edges))

model = None

if model_name == "M3N":
    model = M3N_classifier.M3NClassifier(graph_config)
elif model_name == "M3N_relaxed":
    model = M3N_classifier_relaxed.M3NClassifierRelaxed(graph_config)
elif model_name == "NN_M3N":
    model = NN_M3N.NN_M3N(graph_config)
elif model_name == "NN_M3N_visual":
    model = NN_M3N_visual.NN_M3N_visual(graph_config)
elif model_name == "NN_M3N_lenet":
    model = NN_M3N_lenet.NN_M3N_lenet(graph_config)
elif model_name == "M3N_linear_visual":
    model = M3N_linear_visual.M3NLinearVisual(graph_config)

best_trn_err = np.PINF
best_val_err = np.PINF

best_trn_err_01 = np.PINF
best_val_err_01 = np.PINF

best_tst_err = np.PINF
best_tst_err_01 = np.PINF

'''
if torch.cuda.is_available():
    device = torch.device(0)
else:
    device = 'cpu'
'''

device = 'cpu'
print("device: ", device)

torch.set_num_threads(1)
missing_prob_clean = missing_prob

if train:
    print("training")

    if model_name != "NN_M3N" and model_name != "NN_M3N_visual" and model_name != "NN_M3N_lenet":
        best_trn_err, best_trn_err_01, best_val_err, best_val_err_01 = model.train(train_config, dataset, model_dir_path,
                                                                                   model_name, results_dir_path, edges_eval)

    else:
        missing_cnt = 0
        for (x, y, i) in dataset.trn_data:
            missing_cnt += y.tolist().count(-1)
        missing_prob = missing_cnt / len(dataset.trn_data) / graph_config.objects_num

        print("real missing prob: ", missing_prob)

        fis_1 = []
        fis_2 = []
        for x, y, i in dataset.trn_data:
            fi_1 = torch.zeros((graph_config.labels_num, len(graph_config.edges)), requires_grad=False).to(device)  # TODO
            fi_2 = torch.zeros((graph_config.labels_num, len(graph_config.edges)), requires_grad=False).to(device)  # TODO

            #fi_1 = (torch.rand((graph_config.labels_num, len(graph_config.edges)), requires_grad=False).to(device)-0.5)
            #fi_2 = (torch.rand((graph_config.labels_num, len(graph_config.edges)), requires_grad=False).to(device)-0.5)

            fis_1.append(fi_1)
            fis_2.append(fi_2)

        optimizer = None

        ep = 0

        """

        model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
            train_config.missing_prob)
        model_path = os.path.join(model_dir_path, model_save_name)

        ep = np.load(model_path + "_epoch.npz")
        weights_bin = np.load(model_path + "_bin.npz")
        fis_1 = np.load(model_path + "_fis_1.npz")
        fis_2 = np.load(model_path + "_fis_2.npz")

        ep = ep[ep.files[0]][0]
        fis_1 = fis_1[fis_1.files[0]]
        fis_2 = fis_2[fis_2.files[0]]
        w_b = weights_bin[weights_bin.files[0]]

        fis_1 = [torch.tensor(fi_1, requires_grad=False).to(device) for fi_1 in fis_1]
        fis_2 = [torch.tensor(fi_2, requires_grad=False).to(device) for fi_2 in fis_2]
        model.w_b = torch.tensor(w_b, requires_grad=True).to(device)

        model.load_state_dict(torch.load(model_path + "_Q.pt"))"""








        if optimizer_type == "Adam":
            if model_name == "NN_M3N":
                optimizer = torch.optim.Adam([
                    {'params': model.w_a},
                    {'params': model.w_b},
                    {'params': fis_1},
                    {'params': fis_2}
                ], weight_decay=lbd)
            else:
                optimizer = torch.optim.Adam([
                    {'params': model.parameters()},
                    {'params': model.w_b},
                    {'params': fis_1},
                    {'params': fis_2},
                ], weight_decay=lbd)

        elif optimizer_type == "SGD":
            if model_name == "NN_M3N":
                optimizer = torch.optim.SGD([
                    {'params': model.w_a},
                    {'params': model.w_b},
                    {'params': fis_1},
                    {'params': fis_2}
                ], lr=0.001, momentum=0.9)
            else:
                optimizer = torch.optim.SGD([
                    {'params': model.parameters()},
                    {'params': model.w_b},
                    {'params': fis_1},
                    {'params': fis_2}
                ], lr=0.001, momentum=0.9)

        # optimizer.load_state_dict(torch.load(model_path + "_optimizer.pt"))








        for epoch in range(ep + 1, number_of_training_epochs + 1):
            print("epoch: ", epoch)

            model.train()

            lr = 1 / (100 + epoch)
            for g in optimizer.param_groups:
                g['lr'] = lr

            epoch_loss = 0
            batches = dataset.get_batches(train_config.batch_size)

            for batch in batches:
                #end1 = time.time()
                for (x, y, i) in batch:

                    fi_1 = fis_1[i].to(device)
                    fi_2 = fis_2[i].to(device)
                    #x = x.requires_grad_(True).to(device)  # TODO based on config
                    fi_1.requires_grad_(True)
                    fi_2.requires_grad_(True)

                    Q, G = model(x, device)

                    """if epoch % 100 == 0 and i == 0:
                        f.visualize_weights2(Q.detach().numpy(), graph_config.labels_num, graph_config.objects_num)
                        f.visualize_weights2(G.detach().numpy(), graph_config.labels_num, graph_config.labels_num)"""

                    loss = model.loss(Q, G, y, fi_1, fi_2, lbd, missing_prob, device)
                    epoch_loss += loss.item()
                    loss.backward()

                    #model.w_a.grad /= trn_dataset_size
                    #fi_1.grad *= 10
                    #fi_2.grad *= 10

                    fi_1.requires_grad_(False)
                    fi_2.requires_grad_(False)

                optimizer.step()
                optimizer.zero_grad(set_to_none=True)

                #end2 = time.time()
                #print(end2 - end1)

            print("epoch loss:", epoch_loss/len(dataset.trn_data))

            f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
            file_object = open("live_log/" + f_name + '.txt', 'a')
            file_object.write(str(epoch) + ": ")
            file_object.write(str(epoch_loss/len(dataset.trn_data)) + "\n")
            file_object.close()

            if epoch % train_config.validation_frequency == 0:
                f.visualize_weights2(model.w_a.detach().numpy(), 9, 10)
                f.visualize_weights2(model.w_b.detach().numpy(), 9, 9)
                """print("val check................")
                model.eval()

                trn_err = 0
                trn_err_01 = 0
                # TODO i uz nemusi byt poporade!!!
                for (x, y, i) in dataset.trn_data:
                    if i == 10:  # TODO 10?
                        break

                    Q, G = model(x)
                    Q = Q.detach().numpy().astype(np.float64)
                    G = G.detach().numpy().astype(np.float64)

                    if task == "sudoku":
                        y_out, _ = adag(Q, G, edges_eval)

                    elif task == "hmc":
                        y_out, _ = viterbi(Q, G)

                    hl = f.hamming_loss(y, y_out)
                    trn_err += hl

                    if hl != 0:
                        trn_err_01 += 1

                    print(y)
                    print(y_out)
                    print(hl)

                    num_row = 9
                    num_col = 9
                    fig, axes = plt.subplots(num_row, num_col, figsize=(1.5 * num_col, 2 * num_row))
                    for j in range(9 * 9):
                        ax = axes[j // num_col, j % num_col]
                        ax.imshow(x.detach().numpy()[:, :, j].reshape((28, 28)), cmap='gray')
                        ax.get_xaxis().set_visible(False)
                        ax.get_yaxis().set_visible(False)
                    plt.tight_layout()
                    plt.show()

                    f.visualize_weights2(Q, graph_config.labels_num, graph_config.objects_num)
                    f.visualize_weights2(G, graph_config.labels_num, graph_config.labels_num)

                trn_err = 100 * trn_err / 10  # TODO 10?
                trn_err_01 = 100 * trn_err_01 / 10  # TODO 10?

                val_err = 0
                val_err_01 = 0

                for (x, y) in dataset.val_data:

                    Q, G = model(x)
                    Q = Q.detach().numpy().astype(np.float64)
                    G = G.detach().numpy().astype(np.float64)

                    if task == "sudoku":
                        y_out, en = adag(Q, G, edges_eval)

                    elif task == "hmc":
                        y_out, en = viterbi(Q, G)

                    hl = f.hamming_loss(y, y_out)
                    val_err += hl

                    if hl != 0:
                        val_err_01 += 1

                    print(y)
                    print(y_out)
                    print(hl)

                    en2 = 0
                    en3 = 0

                    for i, yo in enumerate(y_out):
                        en2 += Q[yo, i]

                    for e in model.graph_config.edges:
                        v1 = e[0]
                        v2 = e[1]
                        en2 += G[y_out[v1], y_out[v2]]

                    for i, yo in enumerate(y):
                        en3 += Q[yo, i]

                    for e in model.graph_config.edges:
                        v1 = e[0]
                        v2 = e[1]
                        en3 += G[y[v1], y[v2]]

                    print(en, en2, en3)

                    num_row = 9
                    num_col = 9
                    fig, axes = plt.subplots(num_row, num_col, figsize=(1.5 * num_col, 2 * num_row))
                    for j in range(9 * 9):
                        ax = axes[j // num_col, j % num_col]
                        ax.imshow(x.detach().numpy()[:, :, j].reshape((28, 28)), cmap='gray')
                        ax.get_xaxis().set_visible(False)
                        ax.get_yaxis().set_visible(False)
                    plt.tight_layout()
                    plt.show()

                    f.visualize_weights2(Q, graph_config.labels_num, graph_config.objects_num)
                    f.visualize_weights2(G, graph_config.labels_num, graph_config.labels_num)

                val_err = 100 * val_err / len(dataset.val_data)
                val_err_01 = 100 * val_err_01 / len(dataset.val_data)

                print("training error: ", trn_err)
                print("training error 0/1: ", trn_err_01)

                print("validation error: ", val_err)
                print("validation error 0/1: ", val_err_01)

                print("best training error: ", best_trn_err)
                print("best training error 0/1: ", best_trn_err_01)

                print("best validation error: ", best_val_err)
                print("best validation error 0/1: ", best_val_err_01)

                f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)

                file_object = open("live_log/" + f_name + '.txt', 'a')
                file_object.write("trn: " + str(trn_err) + "\n")
                file_object.write("trn 0/1: " + str(trn_err_01) + "\n")
                file_object.write("val: " + str(val_err) + "\n")
                file_object.write("val 0/1: " + str(val_err_01) + "\n")
                file_object.write("best trn: " + str(best_trn_err) + "\n")
                file_object.write("best trn 0/1: " + str(best_trn_err_01) + "\n")
                file_object.write("best val: " + str(best_val_err) + "\n")
                file_object.write("best val 0/1: " + str(best_val_err_01) + "\n")
                file_object.close()

                save_model = True  # TODO
                save_model_best = True  # TODO

                if trn_err < best_trn_err:
                    best_trn_err = trn_err

                if trn_err_01 < best_trn_err_01:
                    best_trn_err_01 = trn_err_01

                if val_err < best_val_err:
                    best_val_err = val_err

                if val_err_01 < best_val_err_01:
                    best_val_err_01 = val_err_01
                    save_model_best = True  # TODO

                if save_model:
                    model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
                        train_config.missing_prob)
                    model_path = os.path.join(model_dir_path, model_save_name)

                    print(model_path)

                    if model_name == "M3N" or model_name == "M3N_relaxed":
                        np.savez(model_path + "_un", model.w_a)
                        np.savez(model_path + "_bin", model.w_b)

                    elif model_name == "NN_M3N":
                        np.savez(model_path + "_un", model.w_a.detach().numpy())
                        np.savez(model_path + "_bin", model.w_b.detach().numpy())

                    elif model_name == "NN_M3N_visual" or "NN_M3N_lenet":
                        torch.save(model.state_dict(), model_path + "_Q.pt")
                        np.savez(model_path + "_bin", model.w_b.detach().numpy())
                        np.savez(model_path + "_fis_1", [fi_1.detach().numpy() for fi_1 in fis_1])
                        np.savez(model_path + "_fis_2", [fi_2.detach().numpy() for fi_2 in fis_2])

                    res = [best_trn_err, best_trn_err_01, best_val_err, best_val_err_01, best_tst_err, best_tst_err_01]

                    print("saving best result: ", res)

                    with open(os.path.join(results_dir_path,
                                           model_name + "_" + str(trn_dataset_size) + "_" + str(lbd) + "_" + str(
                                                   missing_prob_clean) + ".pkl"), 'wb') as pickle_file:
                        pickle.dump(res, pickle_file)

                if save_model_best:
                    model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
                        train_config.missing_prob)
                    model_path = os.path.join(model_dir_path, model_save_name)

                    torch.save(model.state_dict(), model_path + "_Q_best.pt")
                    np.savez(model_path + "_bin_best", model.w_b.detach().numpy())

                    print("best model saved")"""

                model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
                    train_config.missing_prob)
                model_path = os.path.join(model_dir_path, model_save_name)

                #torch.save(optimizer.state_dict(), model_path + "_optimizer.pt")

                #torch.save(model.state_dict(), model_path + "_Q.pt")
                #torch.save(model.state_dict(), model_path + "_Q" + "_epoch_" + str(epoch) + ".pt")

                #np.savez(model_path + "_bin", model.w_b.detach().numpy())
                np.savez(model_path + "_un" + "_epoch_" + str(epoch), model.w_a.detach().numpy())
                np.savez(model_path + "_bin" + "_epoch_" + str(epoch), model.w_b.detach().numpy())

                #np.savez(model_path + "_fis_1", [fi_1.detach().numpy() for fi_1 in fis_1])
                #np.savez(model_path + "_fis_2", [fi_2.detach().numpy() for fi_2 in fis_2])

                #np.savez(model_path + "_epoch", [epoch])

        print("training finished")
        print("best training error: ", best_trn_err)
        print("best training error 0/1: ", best_trn_err_01)
        print("best validation error: ", best_val_err)
        print("best validation error 0/1: ", best_val_err_01)

if test:

    print("testing")
    tst_err = 0
    tst_err_0_1 = 0

    model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
        train_config.missing_prob)
    model_path = os.path.join(model_dir_path, model_save_name)

    if model_name == 'M3N' or model_name == "M3N_relaxed" or model_name == "NN_M3N":
        weights_un = np.load(model_path + "_un.npz")
        weights_bin = np.load(model_path + "_bin.npz")
        file_names_un = weights_un.files
        file_names_bin = weights_bin.files
        w_a = weights_un[file_names_un[0]]
        w_b = weights_bin[file_names_bin[0]]

    elif model_name == "NN_M3N_visual" or model_name == "NN_M3N_lenet":
        #weights_bin = np.load(model_path + "_bin_best.npz")
        #weights_bin = np.load(model_dir_path + "/NN_M3N_lenet_10_0_0_bin.npz")
        #file_names_bin = weights_bin.files
        #w_b = weights_bin[file_names_bin[0]]
        #model.load_state_dict(torch.load(model_path + "_Q_best.pt"))
        model.load_state_dict(torch.load(model_dir_path + "/NN_M3N_lenet_1000_0_0.5_Q_epoch_200.pt"))
        #model.w_b = w_b

    with torch.no_grad():
        if model_name == "NN_M3N_visual" or model_name == "NN_M3N_lenet":
            model.eval()

        for (x, y) in dataset.tst_data:

            if model_name == "NN_M3N_visual" or model_name == "NN_M3N_lenet":
                Q, G = model(x)
                Q = Q.detach().numpy().astype(np.float64)
                #G = G.detach().numpy().astype(np.float64)

                num_row = 9
                num_col = 9
                fig, axes = plt.subplots(num_row, num_col, figsize=(1.5 * num_col, 2 * num_row))
                for j in range(9 * 9):
                    ax = axes[j // num_col, j % num_col]
                    ax.imshow(x.detach().numpy()[:, :, j].reshape((28, 28)), cmap='gray')
                    ax.get_xaxis().set_visible(False)
                    ax.get_yaxis().set_visible(False)
                plt.tight_layout()
                plt.show()

                f.visualize_weights2(Q, graph_config.labels_num, graph_config.objects_num)
                f.visualize_weights2(G, graph_config.labels_num, graph_config.labels_num)

            elif model_name == 'M3N' or model_name == "M3N_relaxed" or model_name == "NN_M3N":
                Q = np.transpose([w_a[:, i] for i in x])
                G = w_b

            if task == "sudoku":
                y_out, en = adag(Q, G, edges_eval)

            elif task == "hmc":
                y_out, en = viterbi(Q, G)

            hl = f.hamming_loss(y, y_out)

            en2 = 0
            en3 = 0

            for i, yo in enumerate(y_out):
                en2 += Q[yo, i]

            for e in model.graph_config.edges:
                v1 = e[0]
                v2 = e[1]
                en2 += G[y_out[v1], y_out[v2]]

            for i, yo in enumerate(y):
                en3 += Q[yo, i]

            for e in model.graph_config.edges:
                v1 = e[0]
                v2 = e[1]
                en3 += G[y[v1], y[v2]]

            print(hl)
            print(en, en2, en3)

            r = []
            for i, (y1, y2) in enumerate(zip(y.detach().numpy(), y_out)):
                r.append((y1 + 1, y2 + 1))
                if i % 9 == 8:
                    print(r)
                    r = []

            f.visualize_weights2(Q, graph_config.labels_num, graph_config.objects_num)
            f.visualize_weights2(G, graph_config.labels_num, graph_config.labels_num)


            tst_err += hl
            if hl != 0:
                tst_err_0_1 += 1

        best_tst_err = 100 * tst_err / len(dataset.tst_data)
        best_tst_err_01 = 100 * tst_err_0_1 / len(dataset.tst_data)
        print("test error: %f %%" % best_tst_err)
        print("test 0/1 error: %f %%" % best_tst_err_01)

if test2:
    for epoch in range(100, 5001, 100):

        """if os.path.exists(os.path.join(results_dir_path, model_name + "_" +
                                                         str(trn_dataset_size) + "_" + str(lbd) + "_" +
                                                     str(missing_prob_clean) + "_" + str(epoch) + ".pkl")):
            print("exits")
            print(os.path.join(results_dir_path, "trn_" + model_name + "_" + str(trn_dataset_size) + "_" + str(lbd) + "_" +
                                                     str(missing_prob_clean) + "_" + str(epoch) + ".pkl"))
            continue

        f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
        file_object = open("results/" + f_name + '.txt', 'a')
        file_object.write("epoch: " + str(epoch) + "\n")
        file_object.close()

        best_val_err = 0
        best_val_err_01 = 0
        best_tst_err = 0
        best_tst_err_01 = 0

        model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
            train_config.missing_prob)
        model_path = os.path.join(model_dir_path, model_save_name)

        weights_bin = np.load(model_path + "_bin_epoch_" + str(epoch) + ".npz")
        file_names_bin = weights_bin.files
        w_b = weights_bin[file_names_bin[0]]
        model.load_state_dict(torch.load(model_path + "_Q_epoch_" + str(epoch) + ".pt"))
        model.w_b = w_b"""

        with torch.no_grad():
            model.eval()

            for x, y in dataset.val_data:
                Q, G = model(x)
                Q = Q.detach().numpy()

                y_out, en = adag(Q, G, edges_eval)
                hl = f.hamming_loss(y, y_out)
                print(hl)

                f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
                file_object = open("results/" + f_name + '.txt', 'a')
                file_object.write(str(hl) + "\n")
                file_object.close()

                best_val_err += hl
                if hl != 0:
                    best_val_err_01 += 1

            for x, y in dataset.tst_data:
                Q, G = model(x)
                Q = Q.detach().numpy()

                y_out, en = adag(Q, G, edges_eval)
                hl = f.hamming_loss(y, y_out)
                print(hl)

                f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
                file_object = open("results/" + f_name + '.txt', 'a')
                file_object.write(str(hl) + "\n")
                file_object.close()

                best_tst_err += hl
                if hl != 0:
                    best_tst_err_01 += 1

            best_val_err = 100 * best_val_err / len(dataset.val_data)
            best_val_err_01 = 100 * best_val_err_01 / len(dataset.val_data)

            best_tst_err = 100 * best_tst_err / len(dataset.tst_data)
            best_tst_err_01 = 100 * best_tst_err_01 / len(dataset.tst_data)

            f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
            file_object = open("results/" + f_name + '.txt', 'a')
            file_object.write("val: " + str(best_val_err) + "\n")
            file_object.write("val 0/1: " + str(best_val_err_01) + "\n")
            file_object.write("tst: " + str(best_tst_err) + "\n")
            file_object.write("tst 0/1: " + str(best_tst_err_01) + "\n")
            file_object.close()

            res = [best_trn_err, best_trn_err_01, best_val_err, best_val_err_01, best_tst_err, best_tst_err_01]

            print("saving best result: ", res)

            with open(os.path.join(results_dir_path, model_name + "_" + str(trn_dataset_size) + "_" + str(lbd) + "_" +
                                                     str(missing_prob_clean) + "_" + str(epoch) + ".pkl"), 'wb') as pickle_file:
                pickle.dump(res, pickle_file)

if test_trn:
    for epoch in range(100, 5001, 100):

        if os.path.exists(os.path.join(results_dir_path, "trn_" + model_name + "_" +
                                                         str(trn_dataset_size) + "_" + str(lbd) + "_" +
                                                     str(missing_prob_clean) + "_" + str(epoch) + ".pkl")):
            print("exits")
            print(os.path.join(results_dir_path, "trn_" + model_name + "_" + str(trn_dataset_size) + "_" + str(lbd) + "_" +
                                                     str(missing_prob_clean) + "_" + str(epoch) + ".pkl"))
            continue

        f_name = "trn_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
        file_object = open("results/" + f_name + '.txt', 'a')
        file_object.write("epoch: " + str(epoch) + "\n")
        file_object.close()

        best_val_err = 0
        best_val_err_01 = 0
        best_tst_err = 0
        best_tst_err_01 = 0

        model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
            train_config.missing_prob)
        model_path = os.path.join(model_dir_path, model_save_name)

        weights_bin = np.load(model_path + "_bin_epoch_" + str(epoch) + ".npz")
        file_names_bin = weights_bin.files
        w_b = weights_bin[file_names_bin[0]]
        model.load_state_dict(torch.load(model_path + "_Q_epoch_" + str(epoch) + ".pt"))
        model.w_b = w_b

        with torch.no_grad():
            model.eval()

            for x, y, i in dataset.trn_data:
                Q, G = model(x)
                Q = Q.detach().numpy()

                y_out, en = adag(Q, G, edges_eval)
                hl = f.hamming_loss(y, y_out)
                print(hl)

                f_name = "trn_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
                file_object = open("results/" + f_name + '.txt', 'a')
                file_object.write(str(hl) + "\n")
                file_object.close()

                best_val_err += hl
                if hl != 0:
                    best_val_err_01 += 1

            best_val_err = 100 * best_val_err / len(dataset.val_data)
            best_val_err_01 = 100 * best_val_err_01 / len(dataset.val_data)

            best_tst_err = 100 * best_tst_err / len(dataset.tst_data)
            best_tst_err_01 = 100 * best_tst_err_01 / len(dataset.tst_data)

            f_name = "trn_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
            file_object = open("results/" + f_name + '.txt', 'a')
            file_object.write("val: " + str(best_val_err) + "\n")
            file_object.write("val 0/1: " + str(best_val_err_01) + "\n")
            file_object.write("tst: " + str(best_tst_err) + "\n")
            file_object.write("tst 0/1: " + str(best_tst_err_01) + "\n")
            file_object.close()

            res = [best_trn_err, best_trn_err_01, best_val_err, best_val_err_01, best_tst_err, best_tst_err_01]

            print("saving best result: ", res)

            with open(os.path.join(results_dir_path, "trn_" + model_name + "_" + str(trn_dataset_size) + "_" + str(lbd) + "_" +
                                                     str(missing_prob_clean) + "_" + str(epoch) + ".pkl"), 'wb') as pickle_file:
                pickle.dump(res, pickle_file)

