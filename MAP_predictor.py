import numpy as np


class MAPPredictor:

    def __init__(self, num_states, prior_distribution, emission_matrix, transition_matrix):
        self.num_states = num_states
        self.states = np.arange(0, num_states)
        self.prior_distribution = np.log(prior_distribution)
        self.emission_matrix = np.log(emission_matrix)
        self.transition_matrix = np.log(transition_matrix)

    def predict(self, x):

        y = []
        seq_len = len(x)

        values = np.zeros((self.num_states, seq_len))
        directions = np.zeros((self.num_states, seq_len), dtype=int)

        values[:, 0] = self.prior_distribution + self.emission_matrix[:, x[0]]

        for i in range(1, seq_len):
            for s in range(0, self.num_states):
                prev_values = values[:, i-1]
                transition_values = self.transition_matrix[:, s]
                emission_value = self.emission_matrix[x[i], s]
                current_values = prev_values + transition_values
                val = max(current_values)
                ind = np.argmax(current_values)
                values[s, i] = val + emission_value
                directions[s, i] = ind

        last_ind = np.argmax(values[:, -1])
        y.append(last_ind)

        for i in range(0, seq_len-1):
            yi = directions[y[-1], seq_len-1-i]
            y.append(yi)

        y.reverse()
        return y
