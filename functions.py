import matplotlib.pyplot as plt
import numpy as np
import os


def hamming_loss(y_in, y_out):
    cnt = 0
    for y_i, y_o in zip(y_in, y_out):
        if y_i != -1 and y_i != y_o:
            cnt += 1

    return cnt/len(y_in)


def visualize_weights(path):
    print("loading model: ", path)
    weights = np.load(path + ".npz")
    file_names = weights.files
    w = weights[file_names[0]]

    w_unary = w[:900].reshape(30, 30)
    n_unary = np.linalg.norm(w_unary)
    w_binary = w[900:].reshape(30, 30)
    n_binary = np.linalg.norm(w_binary)

    w_unary /= n_unary
    w_binary /= n_binary

    fig, ax = plt.subplots()
    im = ax.imshow(w_unary)

    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    for i in range(30):
        for j in range(30):
            text = ax.text(j, i, round(w_unary[i, j], 3), size=2.5, ha="center", va="center", color="r")

    plt.show()


def visualize_weights2(w, d1, d2):

    w_unary = w
    n_unary = np.linalg.norm(w_unary)
    w_binary = w
    n_binary = np.linalg.norm(w_binary)

    # w_unary /= n_unary
    # w_binary /= n_binary

    fig, ax = plt.subplots()
    im = ax.imshow(w_unary)
    #ax.set(xlabel='labels', ylabel='labels')
    #ax.set_title("Binary functions")

    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    for i in range(d1):
        for j in range(d2):
            text = ax.text(j, i, round(w_unary[i, j], 2), size=2.5, ha="center", va="center", color="r")

    plt.show()

def visualize_weights4(w1, w2, d1, d2):



    fig, (ax1, ax2) = plt.subplots(1, 2)
    im = ax1.imshow(w1)
    ax1.set(xlabel='labels', ylabel='objects')
    ax1.set_title("Unary functions")

    im2 = ax2.imshow(w2)
    ax2.set(xlabel='labels', ylabel='labels')
    ax2.set_title("Binary functions")

    # plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

    for i in range(d1):
        for j in range(d2):
            text = ax1.text(j, i, round(w1[i, j], 2), size=5, ha="center", va="center", color="r")
            text = ax2.text(j, i, round(w2[i, j], 2), size=5, ha="center", va="center", color="r")

    plt.show()



