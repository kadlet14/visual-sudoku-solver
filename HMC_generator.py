import random
import numpy as np


class HMCGenerator:

    def __init__(self):
        self.seq_length = 100
        self.states_num = 30
        self.states = np.arange(0, 30)
        self.prior_distribution = np.ones(30) / 30
        self.E = np.ones((30, 30)) * 3 / 290
        self.T = np.ones((30, 30)) * 3 / 290

        for i in range(30):
            self.E[i, i] = 0.7
            self.T[i, i] = 0.7

    def generate(self):

        x = []
        y = []

        y0 = random.choices(self.states, weights=self.prior_distribution, k=1)[0]
        x0 = random.choices(self.states, weights=self.E[y0, :], k=1)[0]

        y.append(y0)
        x.append(x0)

        for i in range(1, self.seq_length):
            y_i = random.choices(self.states, weights=self.T[y[-1], :])[0]
            x_i = random.choices(self.states, weights=self.E[y_i, :])[0]

            y.append(y_i)
            x.append(x_i)

        return x, y
