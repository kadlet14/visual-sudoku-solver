import sys
from time import time
import numpy as np

import SGD_optimizer
import functions as f
import os
import Adam_optimizer

from manet.maxsum import adag
from manet.maxsum import viterbi

import pickle


class M3NClassifierRelaxed:

    def __init__(self, graph_config):
        self.graph_config = graph_config
        self.w_un = np.zeros((graph_config.labels_num, graph_config.observations_num), dtype=float)
        self.w_bin = np.zeros((graph_config.labels_num, graph_config.labels_num), dtype=float)

    def save(self, path):
        print("saving model: ", path)
        np.savez(path, self.weights)

    def classify(self, x, edges_eval):
        Q = np.transpose([self.w_un[:, i] for i in x])
        G = self.w_bin
        #y = self.viterbi(Q, G, np.zeros(self.graph_config.objects_num, dtype=int) - 1)
        y_out, en = adag(Q, G, edges_eval)
        return y_out

    def psi_un(self, x, y_v):
        s_x = self.graph_config.observations_num
        s_y = self.graph_config.labels_num
        p = np.zeros((s_y, s_x), dtype=float)
        for xi, yi in zip(x, y_v):
            p[yi, xi] += 1  # p[yi, :] += x[:, v]
        return p

    def psi_bin(self, y_vv):
        s_y = self.graph_config.labels_num
        p = np.zeros((s_y, s_y), dtype=float)
        for y0, y1 in y_vv:
            p[y0, y1] += 1
        return p

    def psi_partial_un(self, x, y, missing_prob):
        s_x = self.graph_config.observations_num
        s_y = self.graph_config.labels_num
        p = np.zeros((s_y, s_x), dtype=float)
        for v, (xi, yi) in enumerate(zip(x, y)):
            if yi != -1:
                p[yi, xi] += 1  # p[yi, :] += x[:, v]

        p /= (1 - missing_prob)

        return p

    def psi_partial_bin(self, y, missing_prob):
        s_y = self.graph_config.labels_num
        p = np.zeros((s_y, s_y), dtype=float)
        for e in self.graph_config.edges:
            v1 = e[0]
            v2 = e[1]

            y0 = y[v1]
            y1 = y[v2]

            if y0 != -1 and y1 != -1:
                p[y0, y1] += 1

        p /= (1 - missing_prob)**2

        return p

    def grad_fi(self, y_v, y_vv):
        g_1 = np.zeros((self.graph_config.labels_num, len(self.graph_config.edges)), dtype=float)
        g_2 = np.zeros((self.graph_config.labels_num, len(self.graph_config.edges)), dtype=float)

        for i, e in enumerate(self.graph_config.edges):
            v1 = e[0]
            v2 = e[1]

            y_v1 = y_v[v1]
            y_v2 = y_v[v2]

            y_vv1, y_vv2 = y_vv[i]

            g_1[y_vv1, i] += 1
            g_1[y_v1, i] -= 1

            g_2[y_vv2, i] += 1
            g_2[y_v2, i] -= 1

        return g_1, g_2

    def train(self, train_config, dataset, model_dir_path, model_name, results_dir_path, edges_eval):
        best_val_err = np.PINF
        best_trn_err = np.PINF

        missing_cnt = 0
        for (x, y, i) in dataset.trn_data:
            missing_cnt += y.tolist().count(-1)
        missing_prob = missing_cnt / len(dataset.trn_data) / self.graph_config.objects_num

        print(missing_prob)

        optimizer_un = Adam_optimizer.AdamOptimizer(self.graph_config.labels_num, self.graph_config.observations_num)
        optimizer_bin = Adam_optimizer.AdamOptimizer(self.graph_config.labels_num, self.graph_config.labels_num)

        optimizers_fis_1 = []
        optimizers_fis_2 = []

        fis_1 = []
        fis_2 = []

        for x, y, i in dataset.trn_data:

            #fi_1 = np.random.rand(self.graph_config.labels_num, len(self.graph_config.edges))-0.5
            #fi_2 = np.random.rand(self.graph_config.labels_num, len(self.graph_config.edges))-0.5

            fi_1 = np.zeros((self.graph_config.labels_num, len(self.graph_config.edges)), dtype=float)
            fi_2 = np.zeros((self.graph_config.labels_num, len(self.graph_config.edges)), dtype=float)

            fis_1.append(fi_1)
            fis_2.append(fi_2)

            o1 = Adam_optimizer.AdamOptimizer(self.graph_config.labels_num, len(self.graph_config.edges))
            o2 = Adam_optimizer.AdamOptimizer(self.graph_config.labels_num, len(self.graph_config.edges))

            optimizers_fis_1.append(o1)
            optimizers_fis_2.append(o2)


        for epoch in range(1, train_config.number_of_training_epochs + 1):

            print(epoch)

            batches = dataset.get_batches(train_config.batch_size)

            loss = 0

            for batch in batches:

                grad_un = []
                grad_bin = []

                lr = 1 / (100 + epoch)
                for (x, y, i) in batch:
                    Q = np.transpose([self.w_un[:, i] for i in x])
                    G = self.w_bin

                    fi_1 = fis_1[i]
                    fi_2 = fis_2[i]

                    y_vv = []
                    u_vv = 0
                    psi_vv = 0

                    N = np.zeros((self.graph_config.labels_num, self.graph_config.objects_num), dtype=float)

                    for j, e in enumerate(self.graph_config.edges):
                        v1 = e[0]
                        v2 = e[1]

                        f1 = fi_1[:, j]
                        f2 = fi_2[:, j]

                        N[:, v1] += f1
                        N[:, v2] += f2

                        d1 = np.tile(f1, (self.graph_config.labels_num, 1)).transpose()
                        d2 = np.tile(f2, (self.graph_config.labels_num, 1))

                        ind = np.argmax(d1 + d2 + G)
                        y1 = ind // self.graph_config.labels_num
                        y2 = ind % self.graph_config.labels_num

                        u_vv += f1[y1] + f2[y2] + G[y1, y2]

                        y_vv.append((y1, y2))

                        y1 = y[v1]
                        y2 = y[v2]

                        if y1 != -1 and y2 != -1:
                            psi_vv += G[y1, y2]



                    L = np.ones((self.graph_config.labels_num, self.graph_config.objects_num), dtype=float) / self.graph_config.objects_num

                    for j, aa in enumerate(y):
                        if aa != -1:
                            L[aa, j] = 0

                    y_v = np.argmax(Q + L - N, axis=0)
                    u_v = np.sum(np.max(Q + L - N, axis=0))

                    psi_v = 0
                    for j, aa in enumerate(y):
                        if aa != -1:
                            psi_v += Q[aa, j]

                    grad_un.append(self.psi_un(x, y_v) - self.psi_partial_un(x, y, missing_prob))
                    grad_bin.append(self.psi_bin(y_vv) - self.psi_partial_bin(y, missing_prob))
                    grad_fi_1, grad_fi_2 = self.grad_fi(y_v, y_vv)

                    fis_1[i] = optimizers_fis_1[i].optimize(fi_1, grad_fi_1, epoch, lr)
                    fis_2[i] = optimizers_fis_2[i].optimize(fi_2, grad_fi_2, epoch, lr)

                    loss += u_v + u_vv - psi_v - psi_vv

                g_un = (train_config.lbd * self.w_un + sum(grad_un) / len(grad_un))
                g_bin = (train_config.lbd * self.w_bin + sum(grad_bin) / len(grad_bin))

                self.w_un = optimizer_un.optimize(self.w_un, g_un, epoch, lr)
                self.w_bin = optimizer_bin.optimize(self.w_bin, g_bin, epoch, lr)

            print(loss/len(dataset.trn_data))

            # epoch ended

            if epoch % train_config.validation_frequency == 0:

                f.visualize_weights2(self.w_un, 9, 10)
                f.visualize_weights2(self.w_bin, 9, 9)

                f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
                file_object = open("results/" + f_name + '.txt', 'a')
                file_object.write("epoch: " + str(epoch) + "\n")
                file_object.close()

                best_val_err = 0
                best_val_err_01 = 0
                best_tst_err = 0
                best_tst_err_01 = 0

                model_save_name = model_name + "_" + str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(
                    train_config.missing_prob)
                model_path = os.path.join(model_dir_path, model_save_name)

                for x, y in dataset.val_data:
                    break
                    y_out = self.classify(x, edges_eval)
                    hl = f.hamming_loss(y, y_out)
                    f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
                    file_object = open("results/" + f_name + '.txt', 'a')
                    file_object.write(str(hl) + "\n")
                    file_object.close()

                    best_val_err += hl
                    if hl != 0:
                        best_val_err_01 += 1
                    print(hl)

                for x, y in dataset.tst_data:
                    break
                    y_out = self.classify(x, edges_eval)
                    hl = f.hamming_loss(y, y_out)
                    f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
                    file_object = open("results/" + f_name + '.txt', 'a')
                    file_object.write(str(hl) + "\n")
                    file_object.close()

                    best_tst_err += hl
                    if hl != 0:
                        best_tst_err_01 += 1
                    print(hl)

                best_val_err = 100 * best_val_err / len(dataset.val_data)
                best_val_err_01 = 100 * best_val_err_01 / len(dataset.val_data)

                best_tst_err = 100 * best_tst_err / len(dataset.tst_data)
                best_tst_err_01 = 100 * best_tst_err_01 / len(dataset.tst_data)

                f_name = str(dataset.trn_len) + "_" + str(train_config.lbd) + "_" + str(train_config.missing_prob)
                file_object = open("results/" + f_name + '.txt', 'a')
                file_object.write("val: " + str(best_val_err) + "\n")
                file_object.write("val 0/1: " + str(best_val_err_01) + "\n")
                file_object.write("tst: " + str(best_tst_err) + "\n")
                file_object.write("tst 0/1: " + str(best_tst_err_01) + "\n")
                file_object.close()

                res = [best_val_err, best_val_err_01, best_tst_err, best_tst_err_01]

                print("saving best result: ", res)

                with open(os.path.join(results_dir_path, model_name + "_" + str(train_config.trn_dataset_size) + "_" + str(train_config.lbd) + "_" +
                                                         str(train_config.missing_prob) + "_" + str(epoch) + ".pkl"), 'wb') as pickle_file:
                    pickle.dump(res, pickle_file)

                np.savez(model_path + "_un" + "_epoch_" + str(epoch), self.w_un)
                np.savez(model_path + "_bin" + "_epoch_" + str(epoch), self.w_bin)

        print("training finished")
        print("best training error: ", best_trn_err)
        print("best validation error: ", best_val_err)
        return best_trn_err, best_val_err


    def viterbi(self, Q, G, y):
        labels_num, seq_len = Q.shape
        y_opt = []

        vals = np.zeros((labels_num, seq_len), dtype=float)
        dirs = np.zeros((labels_num, seq_len), dtype=int)

        hamm = np.zeros(labels_num)
        if y[0] != -1:
            hamm = np.ones(labels_num)/seq_len
            hamm[y[0]] = 0

        vals[:, 0] = (Q[:, 0] + hamm)

        for i in range(1, seq_len):
            curr_y = y[i]
            tran = G
            prev_vals = vals[:, i - 1]

            hamm = np.zeros(labels_num)
            if curr_y != -1:
                hamm = np.ones(labels_num)/seq_len
                hamm[curr_y] = 0
            emm = Q[:, i]

            s = tran + prev_vals.reshape(labels_num, 1)

            v = np.max(s, 0) + emm + hamm
            d = np.argmax(s, 0)

            vals[:, i] = v
            dirs[:, i] = d

        last_ind = np.argmax(vals[:, -1])
        y_opt.append(last_ind)

        for i in range(seq_len - 1):
            yi = dirs[y_opt[-1], seq_len - 1 - i]
            y_opt.append(yi)

        y_opt.reverse()

        return y_opt
