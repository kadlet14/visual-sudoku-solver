import numpy as np


class FBPredictor:

    def __init__(self, num_states, prior_distribution, emission_matrix, transition_matrix):
        self.states = np.arange(0, num_states)
        self.prior_distribution = prior_distribution
        self.emission_matrix = emission_matrix
        self.transition_matrix = transition_matrix

    # this function is taken from https://en.wikipedia.org/wiki/Forward%E2%80%93backward_algorithm
    def predict(self, x):

        fwd = []
        f_prev = {}
        for i, observation_i in enumerate(x):
            f_curr = {}
            for st in self.states:
                if i == 0:
                    prev_f_sum = self.prior_distribution[st]
                else:
                    prev_f_sum = sum(f_prev[k] * self.transition_matrix[k, st] for k in self.states)

                f_curr[st] = self.emission_matrix[st, observation_i] * prev_f_sum

            fwd.append(f_curr)
            f_prev = f_curr

        bkw = []
        b_prev = {}
        for i, observation_i_plus in enumerate(reversed(x[1:] + [None])):
            b_curr = {}
            for st in self.states:
                if i == 0:
                    b_curr[st] = 1
                else:
                    b_curr[st] = sum(self.transition_matrix[st, k] * self.emission_matrix[k, observation_i_plus] *
                                     b_prev[k] for k in self.states)

            bkw.insert(0, b_curr)
            b_prev = b_curr

        posterior = []
        for i in range(len(x)):
            posterior.append({st: fwd[i][st] * bkw[i][st] for st in self.states})

        res = []

        for obs in posterior:
            ind = max(obs, key=obs.get)
            res.append(ind)

        return res
