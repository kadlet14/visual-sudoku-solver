import numpy as np


class SGDOptimizer:

    def __init__(self):
        pass

    def optimize(self, w, g, k, lr):

        w_updated = w - lr*g

        return w_updated
