import random
import csv
import pickle
import numpy as np
import torch
import matplotlib.pyplot as plt


class Dataset:

    def __init__(self, miss_prob, trn_len, val_len, tst_len):
        self.trn_data = None
        self.val_data = None
        self.tst_data = None

        self.miss_prob = miss_prob
        self.trn_len = trn_len
        self.val_len = val_len
        self.tst_len = tst_len

    def load_csv(self, path):
        with open(path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            data = []
            for (i, row) in enumerate(csv_reader):
                if i != 0:
                    x = list(map(int, list(row[0])))
                    y = list(map(int, list(row[1])))
                    y = [yy - 1 for yy in y]
                    data.append([x, y])
                if i == self.trn_len + self.val_len + self.tst_len:
                    break
        self.partition_data(data)

    def load_pkl(self, path):
        with open(path, 'rb') as pickle_file:
            data = pickle.load(pickle_file)
        self.partition_data(data)

    def load_pkl_visual(self, path):
        with open(path, 'rb') as pickle_file:
            sudoku, splits = pickle.load(pickle_file)
            trn_split = splits[0]['trn']
            val_split = splits[0]['val']
            tst_split = splits[0]['tst']

            data = []

            for i in val_split:
                el = sudoku[i]
                xx = torch.Tensor(el['X'])
                x = torch.zeros(28, 28, 81)
                for v in range(81):
                    x[:, :, v] = xx[:, v].reshape(28, 28) / 255
                y = torch.Tensor(el['Y'])
                y = y.int()
                data.append([x, y])

            for i in tst_split:
                el = sudoku[i]
                xx = torch.Tensor(el['X'])
                x = torch.zeros(28, 28, 81)
                for v in range(81):
                    x[:, :, v] = xx[:, v].reshape(28, 28) / 255
                y = torch.Tensor(el['Y'])
                y = y.int()
                data.append([x, y])

            for i in trn_split:
                el = sudoku[i]
                xx = torch.Tensor(el['X'])
                x = torch.zeros(28, 28, 81)
                for v in range(81):
                    x[:, :, v] = xx[:, v].reshape(28, 28) / 255
                y = torch.Tensor(el['Y'])
                y = y.int()
                data.append([x, y])

        self.partition_data(data)

    def partition_data(self, data):
        trn_data = data[(self.val_len + self.tst_len):(self.val_len + self.tst_len + self.trn_len)]
        vals = [False, True]
        weights = [1 - self.miss_prob, self.miss_prob]
        k = len(trn_data[0][1])
        trn_data_partial = []
        for (i, xy) in enumerate(trn_data):
            mask = random.choices(vals, weights=weights, k=k)
            x = xy[0]
            y = np.ma.filled(np.ma.masked_array(xy[1], mask), -1)
            trn_data_partial.append([x, y, i])

        self.trn_data = trn_data_partial
        self.val_data = data[0:self.val_len]
        self.tst_data = data[self.val_len:(self.val_len + self.tst_len)]

    def get_batches(self, bs):
        random.shuffle(self.trn_data)
        batches = []
        batch_num = len(self.trn_data)//bs

        for i in range(batch_num):
            batches.append(self.trn_data[i*bs:i*bs + bs])

        if batch_num*bs < len(self.trn_data):
            batches.append(self.trn_data[batch_num*bs:])
            print("uneven batch size !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

        return batches
